﻿using Microsoft.AspNetCore.Mvc;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/statistic")]
    [ApiExplorerSettings(GroupName = "Statistic")]
    public class StatisticalController:ControllerBase
    {
        private readonly IStatisticalHandler _statisticalHandler;

        public StatisticalController( IStatisticalHandler statisticalHandler)
        {
            _statisticalHandler = statisticalHandler;;
        }


        /// <summary>
        /// Thống kê số lượng nhân viên mới theo tháng
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("employee")]
        public async Task<IActionResult> GetStatisticEmployeeByMonth()
        {
            var result = await _statisticalHandler.GetStatisticEmployeeByMonth();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thống kê số lượng nhân viên mới theo phòng ban
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("department")]
        public async Task<IActionResult> GetStatisticEmployeeByDepartment()
        {
            var result = await _statisticalHandler.GetStatisticEmployeeByDepartment();
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Thống kê số lượng đăng kí theo tháng
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("registrationform")]
        public async Task<IActionResult> GetStatisticFormByMonth()
        {
            var result = await _statisticalHandler.GetStatisticFormByMonth();
            return Helper.TransformData(result);
        }

       

    }
}
