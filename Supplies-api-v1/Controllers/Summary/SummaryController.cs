﻿using Microsoft.AspNetCore.Mvc;
using Supplies.Business;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Supplies_api_v1.Controllers.Summary
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/summary")]
    [ApiExplorerSettings(GroupName = "Summary")]
    public class SummaryController : ControllerBase
    {
        private readonly ISummaryHandler _summaryHandler;

        public SummaryController(ISummaryHandler summaryHandler)
        {
            _summaryHandler = summaryHandler; ;
        }


        /// <summary>
        /// Lấy Phòng ban đăng kí nhiều sản phẩm nhất
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("department")]
        public async Task<IActionResult> GetSummaryProductDepartment()
        {
            var result = await _summaryHandler.GetSummaryProductDepartment();
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Sản phẩm được đăng kí số lượng nhiều nhất 
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("product")]
        public async Task<IActionResult> GetSummaryProductForm()
        {
            var result = await _summaryHandler.GetSummaryProductForm();
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tháng/năm có số lượng nhân viên mới nhiều nhất
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("product-month")]
        public async Task<IActionResult> GetSummaryProduct()
        {
            var result = await _summaryHandler.GetSummaryProduct();
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Tháng/năm có số lượng sản phẩm được đăng kí nhiều nhất
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [HttpGet, Route("employee-month")]
        public async Task<IActionResult> GetSummaryEmployee()
        {
            var result = await _summaryHandler.GetSummaryEmployee();
            return Helper.TransformData(result);
        }
    }
}