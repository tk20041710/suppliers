﻿
using Bogus;
using Microsoft.EntityFrameworkCore;
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Data
{
  public  class DBContext :DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) :
            base(options)
        {

        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ListProduct> ListProducts { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<RegistrationForm> RegistrationForms { get; set; }
        public virtual DbSet<DepartmentSummary> DepartmentSummary { get; set; }
        public virtual DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                modelBuilder.HasPostgresExtension("public", "unaccent");                                   
            var unit = new[] { "kg", "bottle", "carton" };
            Faker<Product> productfaker = new Faker<Product>()
                .RuleFor(o => o.Id, f => f.Random.Guid())
                .RuleFor(o => o.Name, f => f.Commerce.Product())
                .RuleFor(o => o.Unit, f => f.PickRandom(unit))
                .RuleFor(o => o.CreatedOnDate, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now));
            var product=    productfaker.Generate(1000);
            modelBuilder.Entity<Product>().HasData(product);

            var random = new Bogus.Randomizer();
            Faker<Department> departmentfaker = new Faker<Department>()
                .RuleFor(o => o.Id, f => f.Random.Guid())
                .RuleFor(o => o.Name, f => f.Commerce.Department())
                .RuleFor(o => o.Leader, f => f.Name.FullName())
                .RuleFor(o => o.CreatedOnDate, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now));
            var department = departmentfaker.Generate(random.Number(10,15))  ;
            modelBuilder.Entity<Department>().HasData(department);


            var role=new[] {"Admin","Employee","Staff"};
            Faker<Employee> employeefaker = new Faker<Employee>()
               .RuleFor(o => o.Id, f => f.Random.Guid())
               .RuleFor(o => o.Name, f => f.Name.FullName())
               .RuleFor(o => o.Email, f => f.Internet.Email())
               .RuleFor(o => o.Phone, f => f.Phone.PhoneNumber())
               .RuleFor(o => o.Address, f => f.Address.StreetAddress())
               .RuleFor(o => o.PassWord, f => f.Internet.Password())
               .RuleFor(o => o.Role, f => f.PickRandom(role))
               .RuleFor(o => o.DepartmentId, f => f.PickRandom(department).Id)
               .RuleFor(o => o.CreatedOnDate, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now));
            var employee = employeefaker.Generate(1000);
            modelBuilder.Entity<Employee>().HasData(employee);

            Faker<RegistrationForm> registrationformfaker = new Faker<RegistrationForm>()
               .RuleFor(o => o.Id, f => f.Random.Guid())
               .RuleFor(o => o.EmployeeId, f => f.PickRandom(employee).Id)
               .RuleFor(o => o.CreatedOnDate, f => f.Date.Between(DateTime.Now.AddMonths(-5), DateTime.Now));
              var registration=registrationformfaker.Generate(1000);
            modelBuilder.Entity<RegistrationForm>().HasData(registration);

            foreach (var item in registration)
            {
                Faker<ListProduct> listproductFaker = new Faker<ListProduct>()
                                                       .RuleFor(u => u.Id, f => f.Random.Guid())
                                                       .RuleFor(u => u.RegistrationFormId, item.Id)
                                                       .RuleFor(u => u.ProductId, f => f.PickRandom(product).Id)
                                                       .RuleFor(u => u.Quantity, f => f.Random.Number(1, 20));
                                             var listproduct=  listproductFaker.Generate(random.Number(5, 10));
                modelBuilder.Entity<ListProduct>().HasData(listproduct);
            }

            Faker<Post> postfaker = new Faker<Post>()
               .RuleFor(o => o.Id, f => f.Random.Guid())
               .RuleFor(o => o.Title, f => f.Lorem.Sentence())
               .RuleFor(o => o.Description, f => f.Lorem.Paragraph());             
            var post = postfaker.Generate(2000);
            modelBuilder.Entity<Post>().HasData(post);
            modelBuilder.Entity<Post>()
                 .HasGeneratedTsVectorColumn(
                     p => p.SearchVector,
                     "english", 
                     p => new { p.Title,p.Description}) 
                 .HasIndex(p => p.SearchVector)                
                 .HasMethod("GIN");

           

            base.OnModelCreating(modelBuilder);

        }

     
    }
}
