﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Supplies.Common;

namespace Supplies.Data
{
    [Table("Department")]
    public class Department: BaseTableDefault
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }

        public  ICollection<Employee> employees { get; set; }
    }
}
