﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Supplies.Data
{
    [Table("RegistrationForm")]
  public  class RegistrationForm: BaseTableDefault
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("EmployeeId")]
        public Guid EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public ICollection<ListProduct> listProducts { get; set; } = new List<ListProduct>();

    }
}
