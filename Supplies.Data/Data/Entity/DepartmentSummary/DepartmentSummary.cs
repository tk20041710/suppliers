﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Data
{
    [Table("DepartmentSummary")]
    public class DepartmentSummary :BaseTableDefault
    {
        [Key]
        public Guid Id { get; set; }
        public Guid DepartmentId { get; set; }
        public string Department { get; set; }
        public int TotalProduct { get; set; }
    }
}
