﻿    using AutoMapper;
using LinqKit;
using Supplies.Common;
using Supplies.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Serilog;
using System.Data;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using OfficeOpenXml;

namespace Supplies.Business
{
    public class DepartmentHandler : IDepartmentHandler
    {
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public DepartmentHandler(DBContext dBContext, IMapper mapper)
        {
            _dBContext = dBContext;
            _mapper = mapper;
        }

        public async Task<Response> Create( DepartmentCreateModel param)
        {
            try
            {
                //Validate
                if (string.IsNullOrEmpty(param.Name) || param.Name.Length > 200)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên bộ phận không được để trống và phải dưới 200 từ");

                if (param.Name == param.Leader)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên bộ phận và người đứng đầu không được giống nhau");

                var department = await _dBContext.Departments.Where(d => d.Name.ToLower() == param.Name.ToLower()).FirstOrDefaultAsync();
                if (department != null)
                {
                    return new ResponseError(HttpStatusCode.BadRequest, "Phòng ban đã tồn tại!!");
                }

                     department = new Department();

                    department.Name =param.Name;

                    department.Leader =param.Leader;

                    department.CreatedOnDate = DateTime.Now;

                _dBContext.Departments.Add(department);

                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<DepartmentViewModel>(department);
                return new ResponseObject<DepartmentViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}",param);
                return new Response<DepartmentViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> Delete(Guid id)
        {
            try
            {
                var department = await _dBContext.Departments.Where(d => d.Id == id).FirstOrDefaultAsync();

                if (department == null) 
                    return new ResponseError(HttpStatusCode.BadRequest, "Bộ phận không tồn tại");

                _dBContext.Departments.Remove(department);

                await _dBContext.SaveChangesAsync();

                return new ResponseDelete(HttpStatusCode.OK, "Xóa thành công", id, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Xóa phòng ban không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetPageAsync( DepartmentQueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _dBContext.Departments.Include(d => d.employees).Where(predicate);

               /*var em = _dBContext.Employees;
                var queryResult = _dBContext.Departments.Where(x => x.employees.Intersect(em).Any());*/

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<Department>,Pagination<DepartmentViewModel>>(data);

                if (result != null)
                {
                    
                    return new ResponsePagination<DepartmentViewModel>(result);
                }
                else
                    return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lấy danh sách phòng ban không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetById(Guid id)
        {

            try
            {

                var department = await _dBContext.Departments.Include(d => d.employees).Where(d => d.Id == id).FirstOrDefaultAsync();

                if (department == null)
                    return new ResponseError(HttpStatusCode.BadRequest, "Bộ phận không tồn tại");

                var result = _mapper.Map<DepartmentViewModel>(department);

                return new ResponseObject<DepartmentViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@Id}", id);
                return new Response<DepartmentViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }


        public async Task<Response> Update( DepartmentUpdateModel param, Guid id)
        {
            try
            {
                //Validate

                if (string.IsNullOrEmpty(param.Name) || param.Name.Length > 200)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên bộ phận không được để trống và phải dưới 200 từ");

                if (param.Name == param.Leader)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên bộ phận và người đứng đầu không được giống nhau");

                //Check Department
                var department = await _dBContext.Departments.Where(d => d.Id == id).FirstOrDefaultAsync();

                if (department == null) return new ResponseError(HttpStatusCode.NotFound, "Bộ phận không tồn tại");

                _mapper.Map(param,department);

                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<DepartmentViewModel>(department);
                return new ResponseObject<DepartmentViewModel>(result);


            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param},Id:@id",param,id);
                return new Response<DepartmentViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }
        private Expression<Func<Department, bool>> BuildQuery( DepartmentQueryModel query)
        {

            var predicate = PredicateBuilder.New<Department>(true);
            if (query.Filter != "{}")
            {
                predicate.And(b => b.Name.ToLower() == query.Filter.ToLower());
            }

            if (!string.IsNullOrWhiteSpace(query.FullTextSearch))
            {
                predicate.And(p => EF.Functions.ToTsVector("english", p.Name + " " + p.Leader)
                .Matches(EF.Functions.PlainToTsQuery("english",query.FullTextSearch)));
               
            }
            return predicate;
        }

        public async Task<Response> Readfile()
        {
            try
            {
                var file = "C:\\Users\\khanh\\Videos\\Captures\\Gereat\\test.xlsx";
                var usersList = new List<Department>();
                    using var package = new ExcelPackage(file);
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();
                    var noOfCol = workSheet.Dimension.End.Column;
                    var noOfRow = workSheet.Dimension.End.Row;

                
                    for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                    {
                        var depart = new Department
                        {
                            Id = Guid.NewGuid(),
                                Name= workSheet.Cells[rowIterator, 1].Value?.ToString(),
                            Leader = workSheet.Cells[rowIterator, 2].Value?.ToString(),

                        };
                        usersList.Add(depart);
                    }            
                return new Response<List<Department>>(usersList);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}
