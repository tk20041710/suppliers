﻿using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public  class StatisticalByMouthModel
    {
        public int Month { get; set; }

        public int Year { get; set; }


        public int Total { get; set; }
    }
    public class ByDepartmentModel
    {
        
        public Guid DepartmentId { get; set; }

        public string Leader { get; set; }

        public string Department { get; set; }


        public int Total { get; set; }
    }
    public class ByProductModel
    {

        public string Product { get; set; }

        public int Total { get; set; }
    }



}
