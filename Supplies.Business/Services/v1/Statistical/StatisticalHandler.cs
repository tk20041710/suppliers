﻿using EntityFramework.Testing;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class StatisticalHandler : IStatisticalHandler
    {
        private readonly DBContext _dBContext;

        public StatisticalHandler(DBContext dBContext)
        {
            _dBContext = dBContext;
        }


        public async Task<Response> GetStatisticEmployeeByDepartment()
        {
            try
            {
                var employee = await _dBContext.Departments
                                                        .Select(x => new ByDepartmentModel
                                                        {
                                                            DepartmentId = x.Id,
                                                            Department = x.Name,
                                                            Leader = x.Leader,
                                                            Total = x.employees.Count()
                                                        }).ToListAsync();


                return new ResponseList<ByDepartmentModel>(employee);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách nhân viên theo phòng ban không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetStatisticEmployeeByMonth()
        {
            try
            {
                var employee = await _dBContext.Employees.ToListAsync();

                int FromYear = employee.Min(e => e.CreatedOnDate.Year);
                int ToYear = employee.Max(e => e.CreatedOnDate.Year);

                int FromMonth = employee.Where(g => g.CreatedOnDate.Year == FromYear).Min(e => e.CreatedOnDate.Month);
                int ToMonth = employee.Where(g => g.CreatedOnDate.Year == ToYear).Max(e => e.CreatedOnDate.Month);

                List<StatisticalByMouthModel> list = new List<StatisticalByMouthModel>();
                for (int i = FromYear; i <= ToYear; i++)
                {
                    if (i != FromYear) FromMonth = 1;
                    for (int j = FromMonth; j <= 12; j++)
                    {
                        if (i == ToYear && j > ToMonth) break;

                        var result = employee.GroupBy(x => new { x.CreatedOnDate.Year, x.CreatedOnDate.Month })
                             .Where(x => x.Key.Year == i && x.Key.Month == j).FirstOrDefault();
                        list.Add(new StatisticalByMouthModel()
                        {
                            Month = j,
                            Year = i,
                            Total = result == null ? 0 : result.Count()
                        });


                    }

                }


                return new ResponseList<StatisticalByMouthModel>(list);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách nhân viên theo tháng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetStatisticFormByMonth()
        {
            try
            {
                var form = await _dBContext.RegistrationForms.ToListAsync();

                int FromYear = form.Min(e => e.CreatedOnDate.Year);
                int ToYear = form.Max(e => e.CreatedOnDate.Year);

                int FromMonth = form.Where(g => g.CreatedOnDate.Year == FromYear).Min(e => e.CreatedOnDate.Month);
                int ToMonth = form.Where(g => g.CreatedOnDate.Year == ToYear).Max(e => e.CreatedOnDate.Month);
                List<StatisticalByMouthModel> list = new List<StatisticalByMouthModel>();
                for (int i = FromYear; i <= ToYear; i++)
                {
                    if (i != FromYear) FromMonth = 1;
                    for (int j = FromMonth; j <= 12; j++)
                    {
                        if (i == ToYear && j > ToMonth) break;

                        var result = form.GroupBy(x => new { x.CreatedOnDate.Year, x.CreatedOnDate.Month })
                             .Where(x => x.Key.Year == i && x.Key.Month == j).FirstOrDefault();
                        list.Add(new StatisticalByMouthModel()
                        {
                            Month = j,
                            Year = i,
                            Total = result == null ? 0 : result.Count()
                        });


                    }

                }

                return new ResponseList<StatisticalByMouthModel>(list);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách đăng kí theo tháng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}
