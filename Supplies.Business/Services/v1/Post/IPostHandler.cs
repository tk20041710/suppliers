﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public interface IPostHandler
    {
        Task<Response> GetPageAsync(PaginationRequest query);
    }
}
