﻿using AutoMapper;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using NpgsqlTypes;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class PostHandler : IPostHandler
    {
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public PostHandler(DBContext dBContext, IMapper mapper)
        {
            _dBContext = dBContext;
            _mapper = mapper;
        }


        public async Task<Response> GetPageAsync(PaginationRequest query)
        {
            try
            {
                 var predicate = BuildQuery(query);

                 var queryResult = _dBContext.Posts
                                  .OrderByDescending(p => EF.Functions.ToTsVector("english", p.Title)                                
                                 .Rank(EF.Functions.PlainToTsQuery("english", query.FullTextSearch)))
                                 .Where(predicate);

                 var data = await queryResult.GetPageAsync(query);

                 var result = _mapper.Map<Pagination<Post>, Pagination<PostViewModel>>(data);

                 if (result != null)
                 {

                     return new ResponsePagination<PostViewModel>(result);
                 }
                 else
                     return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");

        
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lấy danh sách bản tin không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        private Expression<Func<Post, bool>> BuildQuery(PaginationRequest query)
        {
            var predicate = PredicateBuilder.New<Post>(true);

            if (!string.IsNullOrWhiteSpace(query.FullTextSearch))
            {
                predicate.And(p => EF.Functions.ToTsVector("english",EF.Functions.Unaccent(p.Title + " " + p.Description))
                           .Matches(EF.Functions.PlainToTsQuery("english", query.FullTextSearch))
                    || EF.Functions.ToTsVector("english", p.Title + " " + p.Description)
                           .Matches(EF.Functions.PlainToTsQuery("english", query.FullTextSearch)));
            }
            return predicate;
        }
    }
}
