﻿using Newtonsoft.Json;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Supplies.Common;

namespace Supplies.Business
{
    public class EmployeeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; }
        public virtual DepartmentViewModelForEmployee Department { get; set; }

    }
    public class EmployeeCreateUpdateModel
    {
        public Guid DepartmentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; } = "Admin";
    }

    public class EmployeeQueryModel : PaginationRequest
    {

    }
    public class EmployeeViewModelForDepartment
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

    }
}
