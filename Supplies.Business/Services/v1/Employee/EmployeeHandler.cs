﻿using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Supplies.Data;
using AutoMapper;
using System.Linq.Expressions;
using LinqKit;
using System.Net;
using Serilog;
using Microsoft.EntityFrameworkCore;

namespace Supplies.Business
{
    public class EmployeeHandler : IEmployeeHandler
    {
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public EmployeeHandler(DBContext dBContext,IMapper mapper)
        {
            _dBContext = dBContext;

            _mapper = mapper;
        }

        public async Task<Response> Create(EmployeeCreateUpdateModel param)
        {
            try
            {

                //Validate
                if (string.IsNullOrEmpty(param.Name) || param.Name.Length > 200)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên nhân viên không được để trống và phải dưới 200 từ");
           
               
                var employee = new Employee();
                    employee.Name = param.Name;
                    employee.Email = param.Email;
                    employee.PassWord = param.PassWord;
                    employee.Phone = param.Phone;
                    employee.Address = param.Address;
                    employee.DepartmentId = param.DepartmentId;
                    employee.Role = "Employee";

                _dBContext.Employees.Add(employee);
                await _dBContext.SaveChangesAsync();
                var result = _mapper.Map<EmployeeViewModel>(employee);
               
                return new ResponseObject<EmployeeViewModel>(result);

            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}", param);
                return new Response<EmployeeViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> Delete(Guid id)
        {
            
            try
            {
                var employee = await _dBContext.Employees.Where(e => e.Id == id).FirstOrDefaultAsync();

                if (employee == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Nhân viên không tồn tại");

                _dBContext.Employees.Remove(employee);

                await _dBContext.SaveChangesAsync();

                return new ResponseDelete(HttpStatusCode.OK, "Xóa thành công", id, "");


            }
            catch(Exception ex)
            {
                Log.Error(ex, "Xóa nhân viên không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                var employee = await _dBContext.Employees.Include(e=>e.Department).Where(e => e.Id == id).FirstOrDefaultAsync();

                if (employee == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Nhân viên không tồn tại");

                var result = _mapper.Map<EmployeeViewModel>(employee);

                return new ResponseObject<EmployeeViewModel>(result);
            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id: {@Id}", id);
                return new Response<EmployeeViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> GetPageAsync(EmployeeQueryModel query)
        {
           try
            {
                var predicate = BuildQuery(query);

                var queryResult = _dBContext.Employees.OrderBy(e=>e.Name).Include(e=>e.Department).Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<EmployeeViewModel>>(data);

                
                if (result != null)
                {
                    return new ResponsePagination<EmployeeViewModel>(result);
                }
                else
                    return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lấy danh sách nhân viên không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> Update(EmployeeCreateUpdateModel param, Guid id)
        {
           try
            {

                //Validate
                if (string.IsNullOrEmpty(param.Name) || param.Name.Length > 200)
                    return new ResponseError(HttpStatusCode.BadRequest, "Tên nhân viên không được để trống và phải dưới 200 từ");
             
                //Check Employee
                var employee = await _dBContext.Employees.Include(e=>e.Department).Where(e => e.Id == id).FirstOrDefaultAsync();
                if (employee == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Nhân viên không tồn tại");

                _mapper.Map(param, employee);               
                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<EmployeeViewModel>(employee);
            return  new ResponseObject<EmployeeViewModel>(result);

            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param},Id: @Id", param,id);
                return new Response<EmployeeViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }
        private Expression<Func<Employee, bool>> BuildQuery(EmployeeQueryModel query)
        {

            var predicate = PredicateBuilder.New<Employee>(true);
            if (query.Filter != "{}")
            {
                predicate.And(b => b.Name.ToLower() == query.Filter.ToLower());
            }
            return predicate;
        }
    }
}
