﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class BackGroundTask :BackgroundService
    {
        public IServiceProvider Services { get; }
        private readonly ILogger<BackGroundTask> _logger;
        public int Count;

        public BackGroundTask(ILogger<BackGroundTask> logger,IServiceProvider serviceProvider)
        {
            _logger = logger ;
            Services = serviceProvider;

        }

        public async Task DoWorkAsync(CancellationToken cancellationToken)
        {
            Interlocked.Increment(ref Count);
            _logger.LogInformation(
             $"DepartmentSummary working, execution count: {Count}");

            using (var scope = Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IDepartmentSummary>();

                await scopedProcessingService.SummaryDepartment(cancellationToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
           
            await DoWorkAsync(cancellationToken);
        }
    }
}
