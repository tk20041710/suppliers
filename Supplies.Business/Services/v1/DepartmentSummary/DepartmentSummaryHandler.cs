﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public class DepartmentSummaryHandler :IDepartmentSummary
    {
        private readonly DBContext _dBContext;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;
        public DepartmentSummaryHandler(DBContext dBContext, ILogger<DepartmentSummaryHandler> logger)
        {
            _dBContext = dBContext;
            _logger = logger;

        }
        private int count;
        public async Task SummaryDepartment(CancellationToken cancellationToken)
        {
            try
            {

                while (!cancellationToken.IsCancellationRequested)
                {
                    Interlocked.Increment(ref count);
                    _logger.LogInformation( $"DepartmentSummary is working. Count: {count}");
                    
                    var product = await _dBContext.ListProducts
                                                        .GroupBy(x => new { x.RegistrationForm.Employee.Department.Name,x.RegistrationForm.Employee.DepartmentId })
                                                        .Select(x => new ByDepartmentModel
                                                        {
                                                            DepartmentId=x.Key.DepartmentId,
                                                            Department = x.Key.Name,
                                                            Total = x.Count()
                                                        }).OrderByDescending(x => x.Total)
                                                        .ToListAsync();
          
                    _dBContext.DepartmentSummary.RemoveRange(_dBContext.DepartmentSummary.Where(a => true));
                    var max = product.Max(g => g.Total);
                    foreach (var item in product)
                    {
                        if (item.Total == max)
                        {
                            var entity = new DepartmentSummary()
                            {
                                DepartmentId = item.DepartmentId,
                                Department = item.Department,
                                TotalProduct = item.Total,                              

                            };
                         _dBContext.DepartmentSummary.Add(entity);
                        }
                    }
                   _dBContext.SaveChanges();

                    await Task.Delay(TimeSpan.FromMinutes(15));
                }

            }
            catch (Exception ex)
            {
                Log.Error("Lấy phòng ban đăng kí nhiều sản phẩm nhất không thành công");
                Log.Error(ex.Message);
            }
        }
    }
}
