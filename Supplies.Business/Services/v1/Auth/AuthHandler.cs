﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
    public class AuthHandler : IAuthHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public AuthHandler(DBContext dBContext, IMapper mapper,IConfiguration configuration,IHttpContextAccessor httpContextAccessor)
        {
            _dBContext = dBContext;

            _httpContextAccessor = httpContextAccessor;

            _mapper = mapper;

            _configuration = configuration;
        }

        public async Task<Response> Authenticate(LoginModel param)
        {
            try
            {
                var user = await _dBContext.Employees.Include(e=>e.Department).Where(o => o.Email == param.Email).FirstOrDefaultAsync();
                if (user == null)
                    return new ResponseError(HttpStatusCode.NotFound, "Tài khoản không tồn tại");
                if (user.PassWord != param.PassWord)
                    return new ResponseError(HttpStatusCode.BadRequest, "Sai mật khẩu");
                var result = _mapper.Map<LoginViewModel>(user);
                result.Token = Generate(user);
                return new ResponseObject<LoginViewModel>(result);
            }
            catch(Exception ex)
            {
                Log.Error("Param:@Param", param);
                return new Response<LoginViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<Response> Changepassword(ChangePasswordModel param)
        {
           try
            {
                var requestInfo = Helper.GetRequestInfo(_httpContextAccessor.HttpContext.Request);
                var id = requestInfo.Id;

                var user = await _dBContext.Employees.Where(e => e.Id == id).FirstOrDefaultAsync();

                if (user.PassWord != param.OldPassWord)
                    return new ResponseError(HttpStatusCode.BadRequest, "Mật khẩu cũ không đúng!!");

                if (param.NewPassWord != param.RePassWord)
                    return new ResponseError(HttpStatusCode.BadRequest, "Mật khẩu không trùng khớp");

                user.PassWord = param.NewPassWord;

             var status=  await _dBContext.SaveChangesAsync();

                if (status > 0)
                { 
                    return new Response(HttpStatusCode.OK, "Đổi mật khẩu thành công !!");
                }
                else
                {
                    return new Response(HttpStatusCode.OK, "Đỏi mật khẩu thất bại!!");
                }                  

            }
            catch(Exception ex)
            {
                Log.Error("");
                Log.Error("Param: @Param", param);
                return new Response<ChangePasswordModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }

        public string Generate(Employee user)
        {

            if (user != null)
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                var claims = new[]
                {
                     
                new Claim("ID",user.Id.ToString()) ,
                new Claim("Email",user.Email),
                new Claim("Name",user.Name) ,
                new Claim("Role",user.Role) };
                var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                    _configuration["Jwt:Audience"],
                    claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: credentials);
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            return null;

        }
    }
}
