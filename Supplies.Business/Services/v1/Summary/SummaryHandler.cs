﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Supplies.Common;
using Supplies.Data;
using System; 
using System.Collections.Generic;
using System.Linq;
using System.Net;  
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{ 
    public class SummaryHandler : ISummaryHandler
    {
         
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public SummaryHandler(DBContext dBContext, IMapper mapper)
        {
            _dBContext = dBContext;
            _mapper = mapper;
        }
        public async Task<Response> GetSummaryProductDepartment()
        {
            try
            {
                var data = await _dBContext.DepartmentSummary.ToListAsync();
                var reuslt = _mapper.Map<List<SummaryDepartmentViewModel>>(data);
                return new ResponseList<SummaryDepartmentViewModel>(reuslt);

            }
            catch (Exception ex)
            {
                Log.Error("Lấy phòng ban đăng kí nhiều sản phẩm nhất không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

      
        public async Task<Response> GetSummaryProductForm()
        {
            try
            {
                var product = await _dBContext.ListProducts.GroupBy(x => new { x.Product.Name, x.Quantity })
                  
                 .Select(x => new SummaryProductViewModel
                 {
                     Product = x.Key.Name,
                     LastModifiedOnDate=DateTime.Now,
                     TotalProduct = x.Sum(x => x.Quantity)
                 }).OrderByDescending(x => x.TotalProduct).ToListAsync();
                
                var max = product.Max(g => g.TotalProduct);
                List<SummaryProductViewModel> result = product.FindAll(g => g.TotalProduct == max);

                return new ResponseList<SummaryProductViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy số lượng mặt hàng đăng kí nhiều nhất");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetSummaryEmployee()
        {
            try
            {
      
                var employee = await _dBContext.Employees.GroupBy(x => new { x.CreatedOnDate.Year, x.CreatedOnDate.Month })
                                    .OrderByDescending(x => x.Count())
                                    .Select(x => new SummaryViewModel
                                    {
                                        Month = x.Key.Month,
                                        Year = x.Key.Year,
                                        LastModifiedOnDate=DateTime.Now,
                                        Total = x.Count()
                                    }).ToListAsync();
                var max = employee.Max(g => g.Total);

                List<SummaryViewModel> result = employee.FindAll(g => g.Total == max);
                return new ResponseList<SummaryViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy danh sách nhân viên theo tháng không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        } 


        public async Task<Response> GetSummaryProduct()
        {
            try
            {     
               
                var product = await _dBContext.ListProducts.GroupBy(x => new { x.CreatedOnDate.Year, x.CreatedOnDate.Month,x.Quantity })
                                      .OrderByDescending(x => x.Count())
                                    .Select(x => new SummaryViewModel
                                    {
                                        Month = x.Key.Month,
                                        Year = x.Key.Year,
                                        LastModifiedOnDate=DateTime.Now,
                                        Total = x.Sum(x=>x.Quantity)
                                        }).ToListAsync();

                var max = product.Max(g => g.Total);
                List<SummaryViewModel> result = product.FindAll(g => g.Total == max);

                return new ResponseList<SummaryViewModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error("Lấy số lượng sản phẩm được đăng kí nhiều nhất theo tháng năm không thành công");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}
