﻿
using Supplies.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supplies.Business
{
   public interface IRegistrationFormHandler
    {
        Task<Response> GetPageAsync(QueryModel query);
        Task<Response> Create(RegistrationFormCreateUpdateModel param);
        Task<Response> GetById(Guid id);
        Task<Response> Update(RegistrationFormCreateUpdateModel param,Guid id);
        Task<Response> Delete(Guid id);
    }
}
