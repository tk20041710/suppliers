﻿using AutoMapper;
using Supplies.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Supplies.Common;
using Serilog;
using Microsoft.AspNetCore.Http;

namespace Supplies.Business
{
    public class RegistrationFormHandler : IRegistrationFormHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DBContext _dBContext;
        private readonly IMapper _mapper;

        public RegistrationFormHandler(DBContext dBContext, IMapper mapper,IHttpContextAccessor httpContextAccessor)
        {
            _dBContext = dBContext;

            _httpContextAccessor = httpContextAccessor;

            _mapper = mapper;
        }

        public async Task<Response> Create(RegistrationFormCreateUpdateModel param)
        {
            try
            {
                //Validate
                var quantity = param.listProducts.Where(p => p.Quantity <= 0);    
                
                 if (quantity.Count()>0) 
                    return new Response(HttpStatusCode.BadRequest, "Số lượng mặt hàng phải lớn hơn 0");

              //  var requestInfo = Helper.GetRequestInfo(_httpContextAccessor.HttpContext.Request);

              //  param.EmployeeId = requestInfo.Id;
               
                var form= _mapper.Map<RegistrationForm>(param);               

                _dBContext.RegistrationForms.Add(form);
                await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<RegistrationFormCreateUpdateModel>(form);

                return new ResponseObject<RegistrationFormCreateUpdateModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param}", param);
                return new Response<RegistrationFormCreateUpdateModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }


        }


        public async Task<Response> Delete(Guid id)
        {
            try
            {
                var form = await _dBContext.RegistrationForms.Where(rf => rf.Id == id).FirstOrDefaultAsync();

                if (form == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Đăng kí không tồn tại");

                _dBContext.RegistrationForms.Remove(form);

                await _dBContext.SaveChangesAsync();

                return new ResponseDelete(HttpStatusCode.OK, "Xóa thành công", id, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Xóa đăng kí thất bại");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }


        public async Task<Response> GetById(Guid id)
        {
            try
            {
                var form = await _dBContext.RegistrationForms.Include(rf => rf.listProducts).ThenInclude(lp => lp.Product)
                                                              .Include(rf => rf.Employee).ThenInclude(e => e.Department)
                                                              .Where(rf => rf.Id == id)
                                                              .FirstOrDefaultAsync();

                if (form == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Đăng kí không tồn tại");

                var result = _mapper.Map<RegistrationFormViewModel>(form);

                return new ResponseObject<RegistrationFormViewModel>(result);

            }
            catch(Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Id:{@Id}",id);
                return new Response<RegistrationFormViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }


        public async Task<Response> GetPageAsync(QueryModel query)
        {
            try
            {
                var predicate = BuildQuery(query);

                var queryResult = _dBContext.RegistrationForms.OrderBy(e => e.CreatedOnDate).Include(rf => rf.listProducts).ThenInclude(lp => lp.Product)
                                                                .Include(rf => rf.Employee).ThenInclude(e => e.Department)
                                                                    .Where(predicate);

                var data = await queryResult.GetPageAsync(query);

                var result = _mapper.Map<Pagination<RegistrationFormViewModel>>(data);

                if (result != null)
                {
                    return new ResponsePagination<RegistrationFormViewModel>(result);
                }
                else
                    return new ResponseError(HttpStatusCode.NotFound, "Không tìm thấy dữ liệu");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Lấy danh sách đăng kí thất bại");
                return new ResponseError(HttpStatusCode.InternalServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }


        public async Task<Response> Update(RegistrationFormCreateUpdateModel param, Guid id)
        {
            try
            {
                //Validate
                   var quantity = param.listProducts.Where(p => p.Quantity <= 0);     
                
                 if (quantity.Count()>0) 
                    return new Response(HttpStatusCode.BadRequest, "Số lượng mặt hàng phải lớn hơn 0");

                //check registrationId
                var form = await _dBContext.RegistrationForms.Include(rf=>rf.listProducts).Where(rf => rf.Id == id).FirstOrDefaultAsync();

                if (form == null) 
                    return new ResponseError(HttpStatusCode.NotFound, "Đăng kí không tồn tại");

                  _dBContext.ListProducts.RemoveRange(form.listProducts);

                form.EmployeeId = param.EmployeeId;

                form.listProducts =_mapper.Map<ICollection<ListProduct>>(param.listProducts);

                _dBContext.ListProducts.AddRange(form.listProducts);

                    await _dBContext.SaveChangesAsync();

                var result = _mapper.Map<RegistrationFormCreateUpdateModel>(form);

                return new ResponseObject<RegistrationFormCreateUpdateModel>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                Log.Error("Param: {@Param},Id:{@Id}", param, id);
                return new Response<RegistrationFormViewModel>
                {
                    Data = null,
                    Message = ex.Message,
                    Code = HttpStatusCode.InternalServerError
                };
            }
        }


        private Expression<Func<RegistrationForm, bool>> BuildQuery(QueryModel query)
        {

            var predicate = PredicateBuilder.New<RegistrationForm>(true);


            if (query.Filter != "{}")
            {
                predicate.And(b => b.Employee.Name.ToLower()==query.Filter.ToLower());
            }

            if (!string.IsNullOrWhiteSpace(query.FullTextSearch))
            {
                predicate.And(b => b.Employee.Department.Name.Contains(query.FullTextSearch));
            }
            return predicate;
        }
    }
}
